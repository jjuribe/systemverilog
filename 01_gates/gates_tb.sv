`default_nettype none

timeunit 1ns;
timeprecision 1ps;

module gates_tb;
    logic clk, in1, in2;

    gates dut(
        .clk(clk),
        .in1(in1),
        .in2(in2),
        .led1(),
        .led2());

    initial begin
        clk = '0;
        forever #10ns clk = ~clk;
    end

    initial begin
        in1 = 1'b0;
        in2 = 1'b0;
        #25 in1 = 1'b1;
        #25 in2 = 1'b1;
        #25 in1 = 1'b0;
        #45 $stop;
    end

endmodule

`default_nettype wire
